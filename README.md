# SKY test

# About the Application
* Developed using JDK 1.8 and spring boot version 2.0.5.RELEASE
* Maven for managing the build
* Developed using a TDD approach
* Unit tests created using junit and mockito
* Uses JDK provided logging

## Implementation

The ```ParentalControlServiceImpl.java``` has the fail safe method ```isAccessToMovieAllowedForPreferredControlLevel```contains the logic that 
* gets the movie rating from the Movie MetaData service and returns true if the customer can access the movie or not otherwise
* Throws a TitleNotFoundException when the movie cannot be found by the Movie Service

The ```MovieService.java``` is unimplemented as its a third party API

## Unit Test

Unit tests under ```ParentalControlServiceImplTest.java```

## Build & test

* The source code is available on bitbucket and can be cloned using

git clone https://rahuldsuvarna@bitbucket.org/rahuldsuvarna/skytest.git

* To build and run tests use ```mvn clean install```
