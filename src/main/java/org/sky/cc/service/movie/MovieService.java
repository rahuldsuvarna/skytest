package org.sky.cc.service.movie;

import org.sky.cc.service.movie.exception.TechnicalFailureException;
import org.sky.cc.service.movie.exception.TitleNotFoundException;

public interface MovieService {
    String getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException;
}