package org.sky.cc.service.vod;

import org.sky.cc.service.movie.exception.TitleNotFoundException;

public interface ParentalControlService {
    boolean isAccessToMovieAllowedForPreferredControlLevel(final String preferredControlLevel, final String movieId) throws TitleNotFoundException;
}
