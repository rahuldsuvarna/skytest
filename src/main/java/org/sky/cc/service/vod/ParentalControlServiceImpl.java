package org.sky.cc.service.vod;

import org.sky.cc.service.movie.MovieService;
import org.sky.cc.service.movie.exception.TechnicalFailureException;
import org.sky.cc.service.movie.exception.TitleNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ParentalControlServiceImpl implements ParentalControlService {

    private final Logger LOGGER = LoggerFactory.getLogger(ParentalControlServiceImpl.class);
    private final MovieService movieService;
    private final static Map<String, Integer> mOfControlLevelToRating = Stream.of(new Object[][]{
            {"U", 10},
            {"PG", 20},
            {"12", 30},
            {"15", 40},
            {"18", 50},
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Integer) data[1]));

    @Autowired
    public ParentalControlServiceImpl(final MovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * Service method to indicate if a customer is able to access movie based on the current preference settings.
     * This is a fail safe method and therefore will return a false if a comparison between the levels cannot be made.
     * @param preferredControlLevel the control level preference of the customer
     * @param movieId the movieId which the customer wants to watch
     * @return true : If the parental control level of the movie is equal to or less than the customer’s preference
     *         false : if the parental control level of the movie is above the customer’s preference or
     *                 when a comparison cannot be made between the two levels
     * @throws TitleNotFoundException when the movie is not found by the Movie Meta Data service
     */
    public boolean isAccessToMovieAllowedForPreferredControlLevel(final String preferredControlLevel,
                                                                  final String movieId) throws TitleNotFoundException {
        LOGGER.info("invoked isAccessToMovieAllowedForPreferredControlLevel with parameters  preferredControlLevel = {}, movieId = {}",
                preferredControlLevel, movieId);

        boolean accessAllowed;
        try {
            // get the movieRating from the movie service
            // compare the preferred control level and movie control level
            // return true when the movie rating is below or equal to the preferred control level or
            // return false otherwise or when a comparison cannot be made.
            accessAllowed = Optional.ofNullable(movieService.getParentalControlLevel(movieId))
                    .map(movieControlLevel -> isMovieLevelBelowOrEqualToPreferredLevel(movieControlLevel, preferredControlLevel))
                    .orElse(false);
        } catch (TechnicalFailureException e) {
            LOGGER.info("A Technical failure was encountered while processing request returning false");
            accessAllowed = false;
        }
        LOGGER.info("returning with access = {} for movieId = {} and preferredControlLevel = {}", accessAllowed, movieId, preferredControlLevel);
        return accessAllowed;
    }

    /**
     * compare movie control level to preferred control level
     * return true only when movie control level is below preferred control level
     * false for all other conditions including when the control level is unknown to the service.
     */
    private boolean isMovieLevelBelowOrEqualToPreferredLevel(final String movieControlLevel, final String preferredControlLevel) {
        final Integer movieControlLevelRating = mOfControlLevelToRating.getOrDefault(movieControlLevel, 100);
        final Integer preferredControlLevelRating = mOfControlLevelToRating.getOrDefault(preferredControlLevel, -100);
        return movieControlLevelRating.compareTo(preferredControlLevelRating) <= 0;
    }
}
