package org.sky.cc.service.vod;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.sky.cc.service.movie.MovieService;
import org.sky.cc.service.movie.exception.TechnicalFailureException;
import org.sky.cc.service.movie.exception.TitleNotFoundException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ParentalControlServiceImplTest {
    final ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

    @Mock
    private MovieService movieService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

    }

    private void verifyArgumentsToMovieService(final String movieId) throws TitleNotFoundException {
        // verify arguments
        verify(movieService).getParentalControlLevel(argumentCaptor.capture());
        final String capturedArgument = argumentCaptor.getValue();

        //verify movieService was invoked exactly once with the parameters
        verify(movieService, times(1)).getParentalControlLevel(eq(movieId));
        assertThat(capturedArgument, is(movieId));
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_NullParameters_NotAllowed() throws TitleNotFoundException, TechnicalFailureException {
        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        final boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(null, null);

        verifyArgumentsToMovieService(null);
        assertThat(actual, is(false));
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_ParentControlLevelSameAsMovieLevel_IsAllowed() throws TitleNotFoundException, TechnicalFailureException {
        final String movieId = "KIDS_MOVIE_ID";
        String preferredControlLevel = "PG";
        when(movieService.getParentalControlLevel(eq(movieId))).thenReturn(preferredControlLevel);

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        final boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);

        verifyArgumentsToMovieService(movieId);
        assertThat(actual, is(true));
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_ParentControlLevelIsAboveMovieLevel_IsAllowed() throws TitleNotFoundException, TechnicalFailureException {
        final String movieId = "ACTION_MOVIE_ID";
        final String preferredControlLevel = "PG";
        when(movieService.getParentalControlLevel(eq(movieId))).thenReturn(preferredControlLevel);

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        final boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);

        verifyArgumentsToMovieService(movieId);
        assertThat(actual, is(true));
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_ParentControlLevelIsBelowMovieLevel_IsNotAllowed() throws TitleNotFoundException, TechnicalFailureException {
        final String movieId = "HORROR_MOVIE_ID";
        final String movieControlLevel = "18";
        final String preferredControlLevel = "PG";
        when(movieService.getParentalControlLevel(eq(movieId))).thenReturn(movieControlLevel);

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        final boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);

        verifyArgumentsToMovieService(movieId);
        verify(movieService, times(1)).getParentalControlLevel(eq(movieId));
        assertThat(actual, is(false));
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_MovieRatingUnKnown_IsNotAllowed() throws TitleNotFoundException, TechnicalFailureException {
        final String movieId = "HORROR_MOVIE_ID";
        final String movieControlLevel = "UA";
        final String preferredControlLevel = "PG";
        when(movieService.getParentalControlLevel(eq(movieId))).thenReturn(movieControlLevel);

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        final boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);

        verifyArgumentsToMovieService(movieId);
        assertThat(actual, is(false));
    }

    @Test(expected = TitleNotFoundException.class)
    public void testIsAccessToMovieAllowedForPreferredControlLevel_Exception_TitleNotFound() throws TitleNotFoundException, TechnicalFailureException {
        final String movieId = "UNKNOWN_MOVIE_ID";
        String preferredControlLevel = "PG";
        when(movieService.getParentalControlLevel(movieId)).thenThrow(new TitleNotFoundException());

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_MovieServiceReturnsNull_NotAllowed() throws TitleNotFoundException, TechnicalFailureException {
        final String movieId = "UNKNOWN_MOVIE_ID";
        String preferredControlLevel = "PG";
        when(movieService.getParentalControlLevel(movieId)).thenReturn(null);

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);

        verifyArgumentsToMovieService(movieId);
        assertThat(actual, is(false));
    }

    @Test
    public void testIsAccessToMovieAllowedForPreferredControlLevel_TechnicalFailureException_IsNotAllowed() throws TitleNotFoundException, TechnicalFailureException {
        String preferredControlLevel = "PG";
        String movieId = "HORROR_MOVIE_ID";
        when(movieService.getParentalControlLevel(any())).thenThrow(new TechnicalFailureException());

        final ParentalControlServiceImpl parentalControlService = new ParentalControlServiceImpl(movieService);
        final boolean actual = parentalControlService.isAccessToMovieAllowedForPreferredControlLevel(preferredControlLevel, movieId);

        verifyArgumentsToMovieService(movieId);
        assertThat(actual, is(false));
    }
}
